#!/usr/bin/env python3
import caosdb as db

# This creates a very simple data model (if it is not yet present)
# This snippet illustrates the basic way to work with the data model in python.
# If you want to create large models for an initial setup, you might want to
# have a look at https://docs.indiscale.com/caosdb-advanced-user-tools/yaml_interface.html

if len(db.execute_query("FIND date")) == 0:
    date = db.Property(name="date", datatype=db.DATETIME)
    date.insert()

if len(db.execute_query("FIND coefficient")) == 0:
    date = db.Property(name="coefficient", datatype=db.DOUBLE)
    date.insert()

if len(db.execute_query("FIND Project")) == 0:
    pro = db.RecordType(name="Project")
    pro.insert()

if len(db.execute_query("FIND Experiment")) == 0:
    exp = db.RecordType(name="Experiment")
    exp.add_property(name="date", importance=db.RECOMMENDED)
    exp.add_property(name="coefficient", importance=db.RECOMMENDED)
    exp.add_property(name="Project", importance=db.RECOMMENDED)
    exp.insert()
