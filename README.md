# README

## Welcome

This is a small collection of code snippets that may serve as a bases for the 
development of LinkAhead Crawler modules.

The setup is such that makes it very easy to start with it. This also implies
that we are working with files that lie locally (where the script is executed)
and are not accessible by the server.

## Prerequisites
You need to have a [LinkAhead Python Client](https://docs.indiscale.com/caosdb-pylib/) 
and [LinkAhead Python Advanced User Tools](https://docs.indiscale.com/caosdb-advanced-user-tools/) installed.
You need a working connection to a LinkAhead instance. We strongly recommend to use one that
is explicitly intended for testing and not for productive use.

## Setup
You can check out the data model that will be used in `insert_model.py`. You 
may check in your LinkAhead instance that no such model exists (although it may).
You can simply test the crawler by calling `python3 crawl.py data` in the directory 
where this file resides.
Afterwards adjust the data model, the data and the crawler implementation in 
`example_cfood.py` as you like.

## Practice your crawling
After executing `crawl.py` you'll notice that two new experiment records with a
date and a result have been created. However, they are lacking the information
about their project. As an excercise on your way towards developing your own
crawler module, try to implement an project cfood that collects the project
information from the directory name(s) within `data` and creates or updates a
corresponding project record. Afterwards, extend `example_cfood.py` and
`crawl.py` such that the project information is attached to the experiment
records.

## Contributing

Thank you very much to all contributers—[past,
present](https://gitlab.com/linkahead/linkahead/-/blob/main/HUMANS.md), and prospective
ones.

### Code of Conduct

By participating, you are expected to uphold our [Code of
Conduct](https://gitlab.com/linkahead/linkahead/-/blob/main/CODE_OF_CONDUCT.md).

## License

* Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).
