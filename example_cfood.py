#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import caosdb as db
import yaml
from caosadvancedtools.cfood import (AbstractFileCFood, assure_property_is,
                                     fileguide)
from caosadvancedtools.datainconsistency import DataInconsistencyError

try:
    from yaml import CDumper as Dumper
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Dumper, Loader


class ExampleCFood(AbstractFileCFood):
    @classmethod
    def get_re(cls):
        # matches for example `data/2010_TestProject/2019-02-03/result.yml`
        # The following groups are created (values for the above example):
        # - project_identifier: 2010_TestProject
        # - project_year: 2010
        # - project_name: TestProject
        # - date: 2019-02-03
        #
        # Note that the project is not used in the following example (see
        # excercise in the README.md).

        return (r".*/(?P<project_identifier>"
                r"(?P<project_year>\d{4})_?(?P<project_name>((?!/).)*))/"
                r"(?P<date>\d{4}-\d{2}-\d{2})/result.yml")

    def create_identifiables(self):
        self.experiment = db.Record()
        self.experiment.add_parent(name="Experiment")
        self.experiment.add_property(name="date",
                                     value=self.match.group('date'))
        self.identifiables.append(self.experiment)

    def update_identifiables(self):
        with open(fileguide.access(self.crawled_path)) as fi:
            data = yaml.load(fi, Loader=Loader)

        # check that the expected data exits and raise an error otherwise

        if 'coefficient' not in data:
            raise DataInconsistencyError(
                "The measured coefficient of experiment must be given in "
                "the results file! Please check:\n{}\n".format(
                    self.crawled_path))

        # add the data to the experiment Record
        assure_property_is(self.experiment, "coefficient", data['coefficient'])
